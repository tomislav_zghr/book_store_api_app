<?php


namespace App\ApiEntity;


class BooksArray
{

    /**
     * @var Book[]
     */
    private $books;

    /**
     * @param Book[] $books
     */
    public function __construct(
        array $books
    ) {
        $this->books = $books;
    }

    /**
     * @param Book[] $books
     * @return BooksArray
     */
    public function setBooks(array $books): BooksArray
    {
        $this->books = $books;
        return $this;
    }

    public function getBooks() : array
    {
        return $this->books;
    }

}
