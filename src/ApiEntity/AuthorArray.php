<?php


namespace App\ApiEntity;


class AuthorArray
{

    /**
     * @var Author[]
     */
    private $authors;

    /**
     * @param Author[] $authors
     */
    public function __construct(
        array $authors
    ) {
        $this->authors = $authors;
    }

    /**
     * @param Author[] $authors
     * @return AuthorArray
     */
    public function setAuthors(array $authors): AuthorArray
    {
        $this->authors = $authors;
        return $this;
    }

    public function getAuthors() : array
    {
        return $this->authors;
    }

}
