<?php

namespace App\ApiEntity;



use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class Author
{

    private $id;


    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @SerializedName("first_name")
     */
    private $firstName;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $lastName;
    /**
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $birthday;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $gender;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $placeOfBirth;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $biography;

    /**
     * @var BooksArray
     */
    private $books;

    public function __construct()
    {
        $this->books = new BooksArray([]);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;
        return $this;

    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    public function getPlaceOfBirth(): ?string
    {
        return $this->placeOfBirth;
    }

    public function setPlaceOfBirth(string $place_of_birth): self
    {
        $this->placeOfBirth = $place_of_birth;
        return $this;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography($biography): self
    {
        $this->biography = $biography;
        return $this;
    }

    /**
     * @return BooksArray
     */
    public function getBooks(): BooksArray
    {
        return $this->books;
    }

    /**
     * @param BooksArray $books
     */
    public function setBooks(BooksArray $books): void
    {
        $this->books = $books;

    }



}
