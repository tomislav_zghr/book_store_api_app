<?php


namespace App\ApiEntity;


use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;



class Book
{

    ///** @var  */

    /**
     * @var Author
     * @Assert\Type("App\ApiEntity\Author")
     */
    private $author;

    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("datetime")
     */
    private $releaseDate;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("datetime")
     */
    private $updatedAt;

    /**
     * @Assert\Type("datetime")
     */
    private $from;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $isbn;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $format;

    /**
     * @Assert\Type("string")
     */
    private $description;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    private $numberOfPages;

    public function __construct()
    {
        //$this->author = new ArrayCollection();
        $this->author = new Author;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * @param mixed $releaseDate
     */
    public function setReleaseDate($releaseDate): self
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param mixed $isbn
     */
    public function setIsbn($isbn): self
    {
        $this->isbn = $isbn;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format): self
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfPages()
    {
        return $this->numberOfPages;
    }

    /**
     * @param mixed $numberOfPages
     */
    public function setNumberOfPages($numberOfPages): self
    {
        $this->numberOfPages = $numberOfPages;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): self
    {
        $this->from = $from;
        return $this;
    }





    public function getAuthor() : Author
    {
        return $this->author;
    }

    /**
     * @param Author $author
     */
    public function setAuthor(Author $author)
    {
        $this->author = $author;
        //return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): self
    {
        $this->description = $description;
        return $this;
    }




}
