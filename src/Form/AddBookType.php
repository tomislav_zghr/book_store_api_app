<?php



namespace App\Form;

use App\ApiEntity\Author;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\ApiEntity\Book;

class AddBookType extends AbstractType
{
    /** @var array */
    public $data;

    public function __construct()
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder

            ->add('author', ChoiceType::class, [

                'choices' => $options['authors'],
                'choice_value' => 'id',
                'choice_label' => function(?Author $author) {
                    return $author ? strtoupper($author->getFirstName()) . ' ' . strtoupper($author->getLastName()) : '';
                },
                'choice_attr' => function(?Author $author) {
                    return $author ? ['class' => 'author_'.strtolower($author->getFirstName())] : [];
                },

            ])
            ->add('title', TextType::class)
            ->add('from', DateTimeType::class)
            ->add('releaseDate', DateTimeType::class)
            ->add('updatedAt', DateTimeType::class)
            ->add('description', TextareaType::class)
            ->add('isbn', TextType::class)
            ->add('format', TextType::class)
            ->add('numberOfPages', IntegerType::class)
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'authors' => [],
        ]);
        $resolver->setAllowedTypes('authors', 'array');
    }
}
