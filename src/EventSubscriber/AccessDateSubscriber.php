<?php


namespace App\EventSubscriber;

use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class AccessDateSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->setAccessDate($args);
    }

    private function setAccessDate(LifecycleEventArgs $args)
    {
        /** @var User $entity */
        $entity = $args->getObject();

        if (!$entity instanceof User) {
            return;
        }

        $entity->setLastAccessed(new \DateTime());

    }
}
