<?php

namespace App\Command;


use App\Services\ApiClient;
use App\Services\ArrayValidation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\HelperInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class addAuthor extends Command
{
    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var ArrayValidation
     */
    private $arrayValidation;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var HelperInterface */
    private $helper;

    /** @var SymfonyStyle */
    private $io;

    private $authenticationQuestionnaire = [
        'email' => [
            'label' => 'Login Email'
        ],
        'password' => [
            'label' => 'Login password',
            'hidden' => false,
        ],
    ];

    private $authorQuestionnaire = [
        'first_name' => [
            'label' => 'First name'
        ],
        'last_name' => [
            'label' => 'Last name'
        ],
        'birthday' => [
            'label' => 'Birthday',
            'info' => 'Date format: year-month-day (2017-11-02)'
        ],
        'biography' => [
            'label' => 'Biography'
        ],
        'gender' => [
            'label' => 'Gender',
            'choice' => [
                1 => 'male',
                2 => 'female'
            ],
            'normalizer' => 'getLabel'
        ],
        'place_of_birth' => [
            'label' => 'Place of birth'
        ]
    ];

    const  GENDERS  = [
        1 => 'male',
        2 => 'female'
    ];

    public function __construct(ApiClient $apiClient, ArrayValidation $arrayValidation)
    {
        $this->apiClient = $apiClient;
        $this->arrayValidation = $arrayValidation;

        parent::__construct();

    }

    protected function configure()
    {
        $this->setName('app:addauthor')
            ->setDescription('Create new book author')
            ->setHelp('This command allows you to create book author from console')
        ;

    }

    protected function makeQuestion($options){


        $choice = $options['choice'] ?? [];
        if($choice) {
            $getLabel = function ($value) use ($choice) {
                return $choice[$value] ?? '';
            };

            $question = new ChoiceQuestion(
                'Please select your author ' . $options['label'],
                self::GENDERS,
                0
            );

            $question->setNormalizer($getLabel);
            $question->setErrorMessage('Entered value %s is invalid.');

        } else {

            $this->io->note($options['info'] ?? '');
            $question = new Question($options['label'] . ': ', '');
            $question->setHidden($options['hidden'] ?? false);

        }
        return $question;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->input = $input;
        $this->output = $output;
        $this->helper = $this->getHelper('question');

        $output->writeln([
            '========================',
            "Let's create book author",
            '========================',
            '',
        ]);

        $valueValidator = function ($value) {
            if (trim($value) === '') {
                throw new \Exception('Value cannot be empty');
            }

            return $value;
        };

        $data = [];
        $credentials = [];
        $helper = $this->getHelper('question');
        foreach ($this->authenticationQuestionnaire as $index => $options) {
            $question = $this->makeQuestion($options);
            $credentials[$index] = $helper->ask($input, $output, $question);
        }

        foreach ($this->authorQuestionnaire as $index => $options) {
            $question = $this->makeQuestion($options);
            $data[$index] = $helper->ask($input, $output, $question);
        }

        if($this->arrayValidation->validateAuthor($data)->count() === 0) {
            $loginData = $this->apiClient->ApiAuthenticate($credentials['email'], $credentials['password']);

            if($loginData) {
                $this->apiClient->setToken($loginData['token_key']);
                $author = $this->apiClient->addAuthor($data);

                $this->io->success([
                    '============= SUCCESS ===========',
                    json_encode($author, JSON_PRETTY_PRINT),
                    '=================================',
                    '',
                ]);


            } else {
                $this->io->error([
                    '=============ERROR============',
                    'INVALID LOGIN CREDENTIALS',
                    '==============================',
                    '',
                ]);
            }
        } else {
            $this->io->warning([
                '=============WARNING============',
                'DATA NOT VALID',
                '================================',
                json_encode($data, JSON_PRETTY_PRINT),
                '================================',
            ]);
        }

        return 1;

    }
}
