<?php


namespace App\Services;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

class ArrayValidation
{
    /**
     * @var Validation
     */
    private $validation;


    public function __construct()
    {
        $this->validation = Validation::createValidator();
    }


    public function validateLogin($data){


        $constraint = new Assert\Collection([
            'fields' => [
                'token_key' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'user' =>
                    new Assert\Collection([
                        'fields' => [
                            'first_name' => new Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                            'last_name' => new Assert\Length(['allowEmptyString' => false, 'max' => 255])
                            ],
                            'allowExtraFields' => true
                        ]
                    ),
            ],
            'allowExtraFields' => true

        ]);

        return $this->validation->validate($data, $constraint, ['Default', 'custom']);
    }

    public function validateBook($data){


        $constraint = new Assert\Collection([
            'fields' => [
                'author' => new Assert\Collection([
                    'id' => new Assert\Type('numeric')
                ]),
                'title' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'description' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'isbn' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'format' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'number_of_pages' => [
                    new Assert\Type('numeric'),
                ],
                'from' => new Assert\DateTime( ['format' => 'Y-m-d h:i:s']),
                'release_date' => new Assert\DateTime( ['format' => 'Y-m-d h:i:s']),
                'updated_at' => new Assert\DateTime( ['format' => 'Y-m-d h:i:s']),

            ],
            'allowExtraFields' => true
        ]);

        return $this->validation->validate($data, $constraint, ['Default', 'custom']);
    }

    public function validateAuthor($data){


        $constraint = new Assert\Collection([
            'fields' => [
                'first_name' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'last_name' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'birthday' => new Assert\DateTime( ['format' => "Y-m-d"]),
                'biography' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'gender' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],
                'place_of_birth' => [
                    new Assert\Type('string'),
                    new  Assert\Length(['allowEmptyString' => false, 'max' => 255]),
                ],

            ],
            'allowExtraFields' => true
        ]);

        return $this->validation->validate($data, $constraint, ['Default', 'custom']);
    }




}
