<?php


namespace App\Services;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ApiClient
{
    /**
     * @var HttpClient
     */
    public $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var string
     */
    private $apiUrl;

    /** @var string */
    private $token = '';

    /**
     * @var ResponseInterface
     */
    private $response;


    public function __construct(
        RouterInterface $router,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->apiUrl = 'https://symfony-skeleton.q-tests.com';
        $this->httpClient = HttpClient::create();
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    public function setToken($token){
        $this->token = $token;
    }

    public function getToken(){
        return $this->token;
    }

    public function getResponse(){
        return $this->response;
    }

    public function requestData($type, $uri, $param = []){
        try {

            $param['verify_peer'] = 0;
            if($this->token) {
                $param['auth_bearer'] = $this->token;
            }

            $this->response = $this->httpClient->request(
                $type, $uri, $param
            );
            if($this->response->getStatusCode() === 200) {
                $content = $this->response->getContent();
                if($content){
                    return $this->response->toArray();
                }
            } else if($this->response->getStatusCode() === 401){
                return $this->response->getStatusCode();
            } else if($this->response->getStatusCode() === 204){
                return $this->response->getStatusCode();
            } else if($this->response->getStatusCode() === 404){
                return [];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return [];
    }

    public function updateUserApiAuthInfo(User $user, $loginData = []){

        if($user->getToken()) {
            $user->setToken($loginData['token_key']);
        } else {
            $user->setToken($loginData['token_key']);
            $user->setEmail($loginData['user']["email"]);
            $user->setName($loginData['user']["first_name"]);
            $user->setSurname($loginData['user']["last_name"]);
            $this->entityManager->persist($user);

        }
        $this->entityManager->flush();
        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @return string|null
     */
    public function ApiAuthenticate($email, $password) {

        $response = $this->requestData('POST', $this->apiUrl . '/api/token',                 [
            'json' => [
                'email' => $email,
                'password' => $password,
            ]
        ]);

        return  $response;

    }

    public function getAuthors() {
        $response = $this->requestData('GET', $this->apiUrl . '/api/authors');
        return $response;
    }
    public function getAuthor($id) {
        $response = $this->requestData('GET', $this->apiUrl . sprintf('/api/authors/%d', $id));

        return $response;

    }

    public function deleteAuthor($id) {
        $response = $this->requestData('DELETE', $this->apiUrl . sprintf('/api/authors/%d', $id));
        return $response;
    }

    public function deleteBook($id) {
        $response = $this->requestData('DELETE', $this->apiUrl . sprintf('/api/books/%d', $id));
        return $response;
    }

    public function addAuthor($author) {
        $response = $this->requestData('POST', $this->apiUrl . '/api/authors', [
            'json' => $author
        ]);
        return $response;
    }

    public function addBook($book) {
        $response = $this->requestData('POST', $this->apiUrl . '/api/books', [
            'json' => $book
        ]);
        return $response;
    }
}
