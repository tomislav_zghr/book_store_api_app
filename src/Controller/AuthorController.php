<?php


namespace App\Controller;


use App\ApiEntity\Author;
use App\ApiEntity\AuthorArray;
use App\Entity\User;
use App\Services\ApiClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/author")
 */
class AuthorController extends AbstractController
{

    /**
     * @var ApiClient
     */
    private $apiClient;

    public function __construct(ApiClient $apiClient, TokenStorageInterface $tokenStorage)
    {
        $this->apiClient = $apiClient;

        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        $this->apiClient->setToken($user->getToken());
    }

    /**
     * @Route("/", name="app_index")
     */
    public function index(SerializerInterface $serializer): Response
    {
        $authors = $this->apiClient->getAuthors();

        if($authors === 401){
            return $this->redirectToRoute('app_logout');
        }
        $authors = $serializer->deserialize(json_encode(['authors' => $authors]),AuthorArray::class, 'json');

        return $this->render('default/index.html.twig', array(
            'user' => '',
            'authors' => $authors,
            'error' => []
        ));
    }

    /**
     * @Route("/{id}", name="app_author")
     */
    public function author(SerializerInterface $serializer, $id): Response
    {

        $author = $this->apiClient->getAuthor($id);
        //$author['books'] = ['books' => $author['books']];


        if($author === 401){
            return $this->redirectToRoute('app_logout');
        } else if (empty($author)) {
            $this->addFlash(
                'info',
                "Author doesn't exist anymore"
            );
            return $this->redirectToRoute('app_index');
        }

        $author['books']['books'] = $author['books'];
        $author = $serializer->deserialize(json_encode($author),Author::class, 'json');

        return $this->render('default/author.html.twig', array(
            'user' => '',
            'author' => $author,
            'error' => []
        ));
    }

    /**
     * @Route("/delete/{id}", name="app_author_delete")
     */
    public function deleteAuthor($id): Response
    {

        $result = $this->apiClient->deleteAuthor($id);

        if($result === 401){
            return $this->redirectToRoute('app_logout');
        }

        if($result){
            $this->addFlash(
                'success',
                'Author successfully deleted'
            );
        } else {
            $this->addFlash(
                'warning',
                'Error occurred while deleting author'
            );
        }

        return $this->redirectToRoute('app_index');

    }


}
