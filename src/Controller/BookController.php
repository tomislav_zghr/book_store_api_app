<?php


namespace App\Controller;


use App\ApiEntity\Author;
use App\ApiEntity\AuthorArray;
use App\ApiEntity\Book;
use App\Entity\User;
use App\Form\AddBookType;
use App\Services\ApiClient;
use App\Services\ArrayValidation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/book")
 */
class BookController extends AbstractController
{

    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var ArrayValidation
     */
    private $arrayValidation;

    public function __construct(ApiClient $apiClient, ArrayValidation $arrayValidation, TokenStorageInterface $tokenStorage)
    {
        $this->apiClient = $apiClient;
        $this->arrayValidation = $arrayValidation;

        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        $this->apiClient->setToken($user->getToken());

    }

    /**
     * @Route("/delete/{author}/{id}", name="app_book_delete")
     */
    public function deleteBook($author, $id): Response
    {

        $result = $this->apiClient->deleteBook($id);

        if($result === 401){
            return $this->redirectToRoute('app_logout');
        }

        if($result){
            $this->addFlash(
                'success',
                'Book successfully deleted'
            );
        } else {
            $this->addFlash(
                'warning',
                'Error occurred while deleting a book'
            );
        }

        return $this->redirectToRoute('app_author', ['id' => $author]);
    }

    /**
     * @Route("/add", name="add_book")
     */
    public function addBook(Request $request, SerializerInterface $serializer)
    {
        $authors = $this->apiClient->getAuthors();

        if($authors === 401){
            return $this->redirectToRoute('app_logout');
        }

        /** @var AuthorArray $authors */
        $authors = $serializer->deserialize(json_encode(['authors' => $authors]),AuthorArray::class, 'json');

        $task = new Book();

        $form = $this->createForm(AddBookType::class, $task, [
            'authors' => $authors->getAuthors()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $bookData = $form->getData();

            $bookData = $serializer->normalize($bookData);


                $result = $this->apiClient->addBook($bookData);

                if ($result === 401) {
                    return $this->redirectToRoute('app_logout');
                }

                if (!empty($result)) {
                    $this->addFlash('success', 'Book Added');
                } else {
                    $this->addFlash('warning', 'Error occurred while adding a book');
                }
                return $this->redirectToRoute('app_author', ['id' => $bookData['author']['id']]);

        }

        return $this->render('add_book/index.html.twig', [
            'our_form' => $form->createView(),
        ]);
    }

}
