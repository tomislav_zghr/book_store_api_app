<?php


namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class AddAuthorConsoleCommandTest extends KernelTestCase
{
    public function testExecute()
    {

        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:addauthor');
        $commandTester = new CommandTester($command);

        $commandTester->setInputs([
            'tomislav.cipric@gmail.com',
            'QNemaPrekovremenih123',
            'SomeFirstName',
            'SomeLastName',
            '2020-02-19',
            'Some long biography text',
            '1',
            'Metropolis'
        ]);

        $commandTester->execute([]);

        $this->assertContains('============= SUCCESS ===========', $commandTester->getDisplay());

    }
}




