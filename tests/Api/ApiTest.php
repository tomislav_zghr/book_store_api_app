<?php

namespace App\Tests\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Services\ApiClient;
use App\Services\ArrayValidation;
use Symfony\Component\HttpFoundation\Response;

class ApiTest extends WebTestCase
{
    /** @var ApiClient */
    private $apiClient;

    /** @var ArrayValidation */
    private $arrayValidator;

    private $client;

    const URL = 'https://symfony-skeleton.q-tests.com';

    const CREDENTIALS = [
        'email' => 'tomislav.cipric@gmail.com',
        'password' => 'QNemaPrekovremenih123'
    ];

    public function setUp() : void
    {
        $this->client = static::createClient();
        self::bootKernel();
        $this->arrayValidator = self::$container->get('App\Services\ArrayValidation');
        $this->apiClient = self::$container->get('App\Services\ApiClient');

    }

    public function testCheckUrl(){

        $response = $this->apiClient->httpClient->request('GET', self::URL . '/api/authors');
        $this->assertNotEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testLogin (){

        $data = $this->apiClient->ApiAuthenticate(self::CREDENTIALS['email'], self::CREDENTIALS['password']);
        $validation = $this->arrayValidator->validateLogin($data);
        $this->assertSame($validation->count(), 0);
        $this->assertArrayHasKey('token_key', $data);
        $this->apiClient->setToken($data['token_key']);
        $response = $this->apiClient->requestData('GET', self::URL . '/api/authors');
        $this->assertSame(Response::HTTP_OK, $this->apiClient->getResponse()->getStatusCode());
    }

    public function testBookValidation (){

        $data = '
        {
            "author": {
                "id": 34825
            },
            "title": "aaaaaaaaaaa1aaa",
            "from": "2015-01-01 00:00:00",
            "release_date": "2015-01-01 00:00:00",
            "updated_at": "2015-01-01 00:00:00",
            "description": "ddddddd1",
            "isbn": "9780440245919",
            "format": "ddd1",
            "number_of_pages": 1
        }
        ';
        $data = json_decode($data, true);
        $validation = $this->arrayValidator->validateBook($data);
        $this->assertSame($validation->count(), 0);

    }

    public function testAuthorValidation (){

        $data = '
        {
          "first_name": "string1",
          "last_name": "string12",
          "birthday": "2020-02-19",
          "biography": "string dgsdgsdg",
          "gender": "male",
          "place_of_birth": "string sdfsdf"
        }
        ';
        $data = json_decode($data, true);
        $result = $this->arrayValidator->validateAuthor($data);
        $this->assertSame($result->count(), 0);

    }

}
