## Q API APP

### Setup:

#### Instalation
How to Composer: https://getcomposer.org/download/
```console
composer install
```

How to Yarn: https://classic.yarnpkg.com/en/docs/install/
```console
yarn install
```

#### Database

Generate User table 

*I used sqlite for this project*

```console
bin/console doctrine:migrations:migrate
```

#### Generate frontend scripts
```console
yarn run encore dev
```

#### Start the server
```console
symfony server:start -d
```
Enabling TLS
```console
symfony server:ca:install
```

#### PHP server environment
- enable curl extension
- enable openssl extension
- enable sqlite extension
